import csv
import datetime
from customer import Customer
from invoice import Invoice
from invoice_items import Item
from customer_sample import Customer_Sample


with open("input/CUSTOMER_SAMPLE.csv",'r') as from_customer_sample:
    csv_from_customer_sample=csv.reader(from_customer_sample)
    for row in csv_from_customer_sample:
        customer_sample=Customer_Sample(str(row[0]))
       
        with open ("input/CUSTOMER.csv" , 'r') as from_customer:
            csv_from_customer=csv.reader(from_customer)
            for row2 in csv_from_customer:
                customer=Customer(row2[0],row2[1],row2[2])
                if(customer_sample.sample()==customer.customercode()):
                    with open ("output/CUSTOMER.csv", 'a') as f:
                        fieldnames = ['CUSTOMER_CODE','FIRSTNAME', 'LASTNAME']
                        csv_writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t',skipinitialspace=True)
                        csv_writer.writeheader()
                        writer=csv.writer(f)
                        writer.writerow([customer.customer_code])
                        writer.writerow([customer.firstname])
                        writer.writerow([customer.lastname])
                                  
                            
                    with open ("input/INVOICE.csv" , 'r') as from_invoice:
                        csv_from_invoice=csv.reader(from_invoice)
                        for row3 in csv_from_invoice:
                            Inv=Invoice(row3[0],row3[1],row3[2],row3[3])
                            if(Inv.customer()==customer_sample.sample()):
                                with open ("output/INVOICE.CSV", 'a') as f:
                                    fieldnames = ['CUSTOMER_CODE','INVOICE_CODE','AMOUNT','DATE']
                                    csv_writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t',skipinitialspace=True)
                                    csv_writer.writeheader()
                                    writer1=csv.writer(f)
                                    writer1.writerow([Inv.customer_code])
                                    writer1.writerow([Inv.invoice_code])
                                    writer1.writerow([Inv.amount])
                                    writer1.writerow([Inv.date])
                        
                                with open ("input/INVOICE_ITEM.csv" , 'r') as from_invoice_item:
                                    csv_from_invoice_item=csv.reader(from_invoice_item)
                                    for row4 in csv_from_invoice_item:
                                        Inv_item=Item(row4[0],row4[1],row4[2],row4[3])
                                        if(Inv.invoicecode()==Inv_item.invoicecodeitem()):
                                            with open ("output/INVOICE_ITEM.CSV", 'a') as f:
                                                fieldnames = ['INVOICE_CODE','ITEM_CODE','AMOUNT','QUANTITY']
                                                csv_writer = csv.DictWriter(f, fieldnames=fieldnames, delimiter='\t',skipinitialspace=True)
                                                csv_writer.writeheader()
                                                writer2=csv.writer(f)
                                                writer2.writerow([Inv_item.invoice_code])
                                                writer2.writerow([Inv_item.item_code])
                                                writer2.writerow([Inv_item.amount])
                                                writer2.writerow([Inv_item.quantity])

