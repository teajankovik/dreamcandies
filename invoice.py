import csv 

class Invoice:

    def __init__(self,customer_code,invoice_code,amount,date):
        self.customer_code=customer_code
        self.invoice_code=invoice_code
        self.amount=amount
        self.date=date

    def customer(self):
        return self.customer_code

    def invoicecode(self):
        return self.invoice_code